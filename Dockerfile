FROM rootproject/root
COPY . .
RUN apt-get update
RUN apt-get install -y python3-pip
RUN apt-get install -y git
RUN python3 -m pip install -U matplotlib && \
    python3 -m pip install --upgrade pip && \
#    python3 -m pip install pyhf[backends,minuit]
    # Installs the jax wheel compatible with Cuda >= 11.1 and cudnn >= 8.0.5 #note that might need to change to jax_releases.html eventually
    python3 -m pip install --upgrade jax jaxlib==0.3.15+cuda11.cudnn805 -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html && \
#    python3 -m pip install pyhf[tensorflow,torch,minuit]
    python -m pip install --upgrade "git+https://github.com/scikit-hep/pyhf.git#egg=pyhf[tensorflow,torch,minuit]"
